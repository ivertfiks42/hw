package Figures;

// Класс круг
public class Circle implements Figure{
    private int radius;

    /*
        Конструктор класса
        Вводим радиус круга
     */
    public Circle(int radius){
        this.radius = radius;
    }


    /*
        Рассчитываем и возвращаем площадь круга
     */
    @Override
    public double calculateArea() {
        return Math.PI * (Math.pow(radius, 2));
    }

    @Override
    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }
}
