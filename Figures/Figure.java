package Figures;

public interface Figure {
    double calculateArea();
    double calculatePerimeter();
}
