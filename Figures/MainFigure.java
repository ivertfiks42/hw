package Figures;

import java.util.ArrayList;
import java.util.Scanner;

public class MainFigure {
    public static void main(String[] args) {
        Square square = new Square(5);
        Square square1 = new Square(9);
        Square square2 = new Square(15);

        Triangle triangle = new Triangle(5, 15);
        Triangle triangle1 = new Triangle(7, 12);

        Circle circle = new Circle(5);
        Circle circle1 = new Circle(10);

        ArrayList<Figure> figures = new ArrayList<>();

        figures.add(square);
        figures.add(square1);
        figures.add(square2);
        figures.add(triangle);
        figures.add(triangle1);
        figures.add(circle);
        figures.add(circle1);

        System.out.println(calculate(figures));

        System.out.println(circle.calculatePerimeter());

    }

    public static double calculate(ArrayList<Figure> figure)
    {
        int sum = 0;
        for(int i = 0; i < figure.size(); i++){
            sum += figure.get(i).calculateArea();
        }
        return sum;
    }
}
