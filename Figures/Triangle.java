package Figures;

public class Triangle implements Figure{
    private int base;
    private int height;
    public Triangle(int b, int h){
        this.base = b;
        this.height = h;
    }
    @Override
    public double calculateArea() {
        return (base*height)/2;
    }

    @Override
    public double calculatePerimeter() {

        // Находим сторону по теореме Пифагора
        double a = Math.sqrt((Math.pow(height, 2) + (Math.pow(base / 2, 2)) + base));


        // Скалдываем две стороны и основание и возвращаем периметр
        return a + a + base;
    }
}
