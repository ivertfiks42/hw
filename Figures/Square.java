package Figures;

public class Square implements Figure{
    private int side;

    public Square(int side){
        this.side = side;
    }
    @Override
    public double calculateArea() {
        return Math.pow(side, 2);
    }

    @Override
    public double calculatePerimeter(){
        return 4 * side;
    }
}
