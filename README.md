# Home Work (Refactoring)

Обновление 26.07.2023

1. Добавлена функция рассчета периметра фигуры.
2. Добавлен вывод детальной информации об участниках забега.

# Рассчет периметра

В интерфейсе figure создан метод calculatePerimeter который предполагает рассчет периметра
для каждой фигуры по определенной формуле (Для каждой фигуры своя формула). Методы реализованы и
описаны в каждом классе - фигуре. Каждый метод возвращает тип double.

## **Рассчет периметра круга**

Для рассчета периметра круга мы просто умнажаем число PI(3.14) на 2 и умножаем на радиус круга.

## **Рассчет периметра квадрата**

Так как у квадрата все стораны равны мы просто умножаем его сторону на 4.

## **Рассчет периметра треугольника**

Для рассчета периметра треугольника нужно сначала найти длинну одной стороны, для этого
используем теорему пифагора и подставляем значения. Далее складываем две стороны и основание треугольника.

## Дополнительная информация об участниках

В класс-родитель Member был добавлен метод displayInfo который выводит информацию об участниках.
Также теперь учитывается возраст участников.