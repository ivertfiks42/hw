package Members;

import java.util.ArrayList;

public class GeneralRun {

    public static void main(String[] args) {
        ArrayList<Member> members = new ArrayList<>();
        ArrayList<Obstacle> obstacles = new ArrayList<>();

        Human human = new Human("Усейн Болт", 35, 210, 36);
        Cat cat = new Cat("Мурзик", 3, 160, 25);
        Robot robot = new Robot("Т-1000", 4, 2000, 60);

        members.add(human);
        members.add(cat);
        members.add(robot);

        Treadmill treadmill1 = new Treadmill("забег - детская", 100);
        Treadmill treadmill2 = new Treadmill("забег - любительская", 150);
        Treadmill treadmill3 = new Treadmill("забег - сложная", 200);

        Wall wall1 = new Wall("стена - детская", 20);
        Wall wall2 = new Wall("стена - любительская", 30);
        Wall wall3 = new Wall("стена - сложная", 40);

        obstacles.add(treadmill1);
        obstacles.add(wall1);
        obstacles.add(treadmill2);
        obstacles.add(wall2);
        obstacles.add(wall3);
        obstacles.add(treadmill3);

        System.out.println("Первый участник: ");
        human.displayInfo();
        System.out.println("Второй участник: ");
        cat.displayInfo();
        System.out.println("Третий участник: ");
        robot.displayInfo();

        for(int i = 0; i < members.size(); i++){
            int distance = 0;
            for(int j = 0; j < obstacles.size(); j++){
                if(obstacles.get(j).getClass().getSimpleName().equals("Treadmill")){
                    if (obstacles.get(j).canOvercome(members.get(i))) {
                        distance += obstacles.get(j).getHeightOrLength();
                        System.out.println("Участник " + members.get(i).getName() + " прошел приграду " + "\"" + obstacles.get(j).getNameOfObstacle() + "\"" + " на дистанции " + distance);
                    } else {
                        System.out.println(members.get(i).getClass().getSimpleName() + " не смог пробежать такой длинный маршрут и выбывает из нашего соревнования");
                        break;
                    }
                }else if(obstacles.get(j).getClass().getSimpleName().equals("Wall")){
                    if (obstacles.get(j).canOvercome(members.get(i))) {
                        System.out.println("Участник " + members.get(i).getName() + " прошел приграду " + "\"" + obstacles.get(j).getNameOfObstacle() + "\"" + " на дистанции " + distance);
                    } else {
                        System.out.println("Участник " + members.get(i).getName() + " не прошел приграду " + "\"" + obstacles.get(j).getNameOfObstacle() + "\"" + " на дистанции " + distance);
                        break;
                    }
                }
            }
        }
    }

}
