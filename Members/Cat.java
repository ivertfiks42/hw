package Members;

public class Cat extends Member {
    public Cat(String name, int age, int maxDistance, int maxJumpHeight){
        super.setName(name);
        super.setAge(age);
        super.setMaxDistance(maxDistance);
        super.setMaxJumpHeight(maxJumpHeight);
    }
    @Override
    public void run() {
        System.out.println("Котик" + " пробежал беговую дорожку");
    }
    @Override
    public void jump() {
        System.out.println("Котик" + "перепрыгнул припятствие");
    }
}


