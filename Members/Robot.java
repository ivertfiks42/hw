package Members;

public class Robot extends Member{
    public Robot(String name, int age, int maxDistance, int maxJumpHeight){
        super.setName(name);
        super.setAge(age); // Только собрали за заводе
        super.setMaxDistance(maxDistance);
        super.setMaxJumpHeight(maxJumpHeight);
    }
    @Override
    public void run() {
        System.out.println("Робот" + " пробежал беговую дорожку");
    }
    @Override
    public void jump() {
        System.out.println("Робот" + "перепрыгнул припятствие");
    }
}
