package Members;

public class Human extends Member{
    public Human(String name, int age, int maxDistance, int maxJumpHeight){
        super.setName(name);
        super.setAge(age);
        super.setMaxDistance(maxDistance);
        super.setMaxJumpHeight(maxJumpHeight);
    }
    @Override
    public void run() {
        System.out.println("Человек" + " пробежал беговую дорожку");
    }
    @Override
    public void jump() {
        System.out.println("Человек" + "перепрыгнул припятствие");
    }
}
