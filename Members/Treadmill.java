package Members;

public class Treadmill implements Obstacle{
    private int length;
    private String name;

    //Конструктор класса Treadmill.
    public Treadmill(String name, int length){
        this.name = name;
        this.length = length;
    }

    // Получить длину беговой дорожки.
    @Override
    public int getHeightOrLength() {
        return length;
    }

    // Установить длину беговой дорожки.
    public void setWidth(int width) {
        this.length = width;
    }
    @Override
    public String getNameOfObstacle() {
        return name;
    }

    // Проверяет, может ли участник преодолеть беговую дорожку.
    @Override
    public boolean canOvercome(Member member) {
        return member.getMaxDistance() > this.length;
    }
}
