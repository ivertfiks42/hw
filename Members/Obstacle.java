package Members;

public interface Obstacle {

    // Проверяет, сможет ли участник преодолеть данное препятствие.

    boolean canOvercome(Member member);

    // Возвращает имя данного препятствия.

    String getNameOfObstacle();

    // Возвращает высоту или длину данного препятствия (в зависимости от типа препятствия).

    int getHeightOrLength();
}
