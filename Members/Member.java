package Members;

public abstract class Member {
    private int maxDistance;
    private int maxJumpHeight;
    private String name;
    private int age;
    public abstract void jump();
    public abstract void run();
    public int getAge() {
        return age;
    }
    protected void setAge(int age) {
        this.age = age;
    }
    public void displayInfo() {
        System.out.println("Имя: " + name);
        System.out.println("Возраст: " + age);
        System.out.println("Максимальная дистанция: " + maxDistance);
        System.out.println("Максимальная высота прыжка: " + maxJumpHeight);
        System.out.println("\n");
    }
    protected void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }
    public int getMaxDistance() {
        return maxDistance;
    }
    protected void setMaxJumpHeight(int maxJumpHeight) {
        this.maxJumpHeight = maxJumpHeight;
    }
    public int getMaxJumpHeight() {
        return maxJumpHeight;
    }
    public String getName() {
        return name;
    }
    protected void setName(String name) {
        this.name = name;
    }
}
