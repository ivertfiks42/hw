package Members;

public class Wall implements Obstacle{
    private int height;
    private String name;

    // Конструктор класса Wall.
    public Wall(String name, int height){
        this.name = name;
        this.height = height;
    }
    @Override
    public String getNameOfObstacle() {
        return name;
    }

    // Получить высоту препятствия.
    public int getHeightOrLength() {
        return height;
    }


    // Проверяет, может ли участник перепрыгнуть препятствие.
    @Override
    public boolean canOvercome(Member member) {
        return member.getMaxJumpHeight() > this.height;
    }
}
